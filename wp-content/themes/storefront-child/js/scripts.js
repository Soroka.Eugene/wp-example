$(document).ready(function(){
  $('.main-slider').slick({
  	infinite: false,
  	adaptiveHeight: true
  });

  $('.content-slider-inner').slick({
  	infinite: false,
  	adaptiveHeight: true,
  	dots: true,
		prevArrow: '<button type="button" class="btn  content-slider-btn-prev"><i class="fas fa-angle-left"></i></button>',
		nextArrow: '<button type="button" class="btn  content-slider-btn-next"><i class="fas fa-angle-right"></i></button>'
  });
});