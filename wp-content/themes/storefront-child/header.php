<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php// do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">
	<?php// do_action( 'storefront_before_header' ); ?>

	<header id="masthead" class="page-header" role="banner">
		<div class="container container-middle">
			<div class="row  page-header-top">
				<div class="col-md-2">
					<?php the_custom_logo(); ?>
				</div>
				
				<div class="col-md-7">
					<?php
					/**
					 * Functions hooked into storefront_header action
					 *
					 * @hooked storefront_header_container                 - 0
					 * @hooked storefront_skip_links                       - 5
					 * @hooked storefront_social_icons                     - 10
					 * @hooked storefront_site_branding                    - 20
					 * @hooked storefront_secondary_navigation             - 30
					 * @hooked storefront_product_search                   - 40
					 * @hooked storefront_header_container_close           - 41
					 * @hooked storefront_primary_navigation_wrapper       - 42
					 * @hooked storefront_primary_navigation               - 50
					 * @hooked storefront_header_cart                      - 60
					 * @hooked storefront_primary_navigation_wrapper_close - 68
					 */
					/*do_action( 'storefront_header' ); */?>
					<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
				</div>

				<div class="col-md-3">
					<div class="user-bar">
						<div class="user-bar-links">
							<a href="#" class="link user-bar-link">Log In</a>
							<a href="#" class="link user-bar-link">Sign In</a>
						</div>

						<div class="user-bar-btn">
							<button class="btn user-bar-btn cart-btn">
								<i class="fas fa-shopping-bag"></i>
								<span>2</span>
							</button>
							<button class="btn user-bar-btn search-btn"></button>
						</div>
					</div>
				</div>
			</div>

			<div class="page-header-bottom row">
				<div class="col">
					<section class="main-slider">
						<div class="main-slider-slide">
							<div class="slide-content">
								<div class="slide-img-wrapper">
									<img src="<?php echo bloginfo('stylesheet_directory')?>/img/girl-slider.png" alt="Slide">
								</div>

								<div class="slide-centerize">
									<h2 class="slide-title">New arrival</h2>
									<div class="slide-btn-wrap">
										<a href="#" class="slide-btn">Buy now</a>
									</div>
								</div>

								<div class="bg-span">Fasion</div>
							</div>
						</div>

						<div class="main-slider-slide">
							
							<div class="slide-content">
								<div class="slide-img-wrapper">
									<img src="<?php bloginfo('stylesheet_directory')?>/img/girl-slider.png" alt="Slide">
								</div>

								<div class="slide-centerize">
									<h2 class="slide-title">New arrival</h2>
									<div class="slide-btn-wrap">
										<a href="#" class="slide-btn">Buy now</a>
									</div>
								</div>

								<div class="bg-span">Fasion</div>
							</div>
						
						</div>

						<div class="main-slider-slide">
							
							<div class="slide-content">
								<div class="slide-img-wrapper">
									<img src="<?php echo bloginfo('stylesheet_directory')?>/img/girl-slider.png" alt="Slide">
								</div>

								<div class="slide-centerize">
									<h2 class="slide-title">New arrival</h2>
									<div class="slide-btn-wrap">
										<a href="#" class="slide-btn">Buy now</a>
									</div>
								</div>

								<div class="bg-span">Fasion</div>
							</div>
						
						</div>
					</section>
				</div>
			</div>
		</div>

	</header><!-- #masthead -->

	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 * @hooked woocommerce_breadcrumb - 10
	 */
	//do_action( 'storefront_before_content' ); ?>

	<div id="content" class="site-content" tabindex="-1">

		<?php
		//do_action( 'storefront_content_top' );
