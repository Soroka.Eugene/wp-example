<?php 
function load_scripts(){

	wp_enqueue_style('bootstrap-css', get_stylesheet_directory_uri().'/css/bootstrap/bootstrap.css');
	wp_enqueue_style('slick-css', get_stylesheet_directory_uri().'/libs/slick/slick.css');
	wp_enqueue_style('ionicons', "https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css");
	wp_enqueue_style('font-awesome', "https://use.fontawesome.com/releases/v5.1.0/css/all.css");
	wp_enqueue_style('google-fonts', "https://fonts.googleapis.com/css?family=Playfair+Display:400,700");
	wp_enqueue_style('custom-css', get_stylesheet_directory_uri().'/css/styles.css');
	wp_deregister_script( 'jquery-core' );
	wp_register_script( 'jquery-core', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js');
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri().'/js/bootstrap/bootstrap.js');
	wp_enqueue_script('slick-js', get_stylesheet_directory_uri().'/libs/slick/slick.min.js');
	//wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.3.1.min.js');
	//wp_enqueue_script('jquery-migrate', 'https://code.jquery.com/jquery-migrate-1.4.1.js');
	wp_enqueue_script('custom-js', get_stylesheet_directory_uri().'/js/scripts.js');

}

add_action('wp_enqueue_scripts', 'load_scripts');

add_action( 'after_setup_theme', 'theme_register_nav_menu' );
function theme_register_nav_menu() {
	register_nav_menu( 'primary', 'Custom Primary Menu' );
}

function wpcustom_deregister_scripts_and_styles(){
    //wp_deregister_style('storefront-woocommerce-style');
    wp_deregister_style('storefront-style');
}
add_action( 'wp_print_styles', 'wpcustom_deregister_scripts_and_styles', 100 );

//remove_action( 'homepage', 'storefront_homepage_content', 10 );
?>