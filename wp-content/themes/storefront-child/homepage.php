<?php
/**
 * Template Name: Example Template 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package storefront
 */

get_header(); ?>

		<main id="main" class="site-main" role="main">

		<?php /*if ( have_posts() ) :

			get_template_part( 'loop' );

		else :

			get_template_part( 'content', 'none' );

		endif; */?>

			<section class="features container">
				<div class="row">
					
				<div class="col-lg-3  col-md-6  feature">
					<div class="feature-content  feature-content-plane">
						<h3 class="feature-title">
							Free shiping
						</h3>
						<p class="feature-description">
							Orders over 99$
						</p>
					</div>
				</div>

				<div class="col-lg-3  col-md-6  feature">
					<div class="feature-content  feature-content-return">
						<h3 class="feature-title">
							30 days return
						</h3>
						<p class="feature-description">
							If goods have problem
						</p>
					</div>
				</div>

				<div class="col-lg-3  col-md-6  feature">
					<div class="feature-content feature-content-payment">	
						<h3 class="feature-title">Secure payment</h3>
						<p class="feature-description">
							100% secure payment
						</p>
					</div>
				</div>

				<div class="col-lg-3 col-md-6  feature">
					<div class="feature-content feature-content-support">
					<h3 class="feature-title">24h Support</h3>
					<p class="feature-description">Dedicated Support</p>
					</div>
				</div>
			
				</div>
			</section>




			<section class="home-shop  container">
				<div class="row  justify-content-md-center">
					<div class="col-lg-3  col-md-5">
						<div class="popular">
							<h3 class="popular-title">
								Popular product
							</h3>

							<a href="$" class="popular-more-link">
								More product
								<i class="fas fa-arrow-right"></i>
							</a>
						</div>
					</div>

					<div class="col-lg-3  col-md-5">
						<article class="shop-product sale">
								<div class="shop-product-img">
									<a href="#" class="shop-link">
										<img src="<?php  echo get_stylesheet_directory_uri(); ?>/img/product-1.png" alt="product-1">
									</a>
								</div>

								<div class="shop-product-title">
									<a href="#" class="shop-link">
										Essential cotton-blend
									</a>
								</div>

								<div class="shop-product-price">
									<span class="shop-product-price-old"><del>$200.00 <span class="uppercase">USD</span></del></span>
									<span class="shop-product-price-actual">$100.00 <span class="uppercase">USD</span></span>
								</div>
						</article>
					</div>

					<div class="col-lg-3  col-md-5">
						<article class="shop-product">
								<div class="shop-product-img">
									<a href="#" class="shop-link">
										<img src="<?php  echo get_stylesheet_directory_uri(); ?>/img/product-2.png" alt="product-2">
									</a>
								</div>

								<div class="shop-product-title">
									<a href="#" class="shop-link">
										Flecked cotton-blend
									</a>
								</div>

								<div class="shop-product-price">
									<span class="shop-product-price-actual">$175.00 <span class="uppercase">USD</span></span>
								</div>
						</article>
					</div>

					<div class="col-lg-3  col-md-5">
						<article class="shop-product">
								<div class="shop-product-img">
									<a href="#" class="shop-link">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product-3.png" alt="product-3">
									</a>
								</div>

								<div class="shop-product-title">
									<a href="#" class="shop-link">
										Striped cotton t-short
									</a>
								</div>

								<div class="shop-product-price">
									<span class="shop-product-price-actual">$200.00 <span class="uppercase">USD</span></span>
								</div>
						</article>
					</div>
				</div>


				<div class="row  justify-content-md-center">
					<div class="col-lg-3  col-md-5">
						<article class="shop-product">
								<div class="shop-product-img">
									<a href="#" class="shop-link">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product-4.png" alt="product-4">
									</a>
								</div>

								<div class="shop-product-title">
									<a href="#" class="shop-link">
										V-neck cotton t-short
									</a>
								</div>

								<div class="shop-product-price">
									<span class="shop-product-price-actual">$100.00 <span class="uppercase">USD</span></span>
								</div>
						</article>
					</div>

					<div class="col-lg-9  col-md-5">
						<div class="shop-new">
							<div class="shop-new-inner">
								<div class="shop-new-jaw">
									Lifestyle
								</div>

								<h3 class="shop-new-title">
									New Now: Stripped cotton
								</h3>

								<div class="shop-new-btn-wrap">
									<a href="#" class="shop-new-btn">
										<span class="shop-new-btn-left">
											$ 50.00
										</span>

										<span class="shop-new-btn-right">
											buy now
										</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row  justify-content-md-center">
					<div class="col-lg-3  col-md-5">
						<article class="shop-product">
								<div class="shop-product-img">
									<a href="#" class="shop-link">
										<img src="<?php  echo get_stylesheet_directory_uri(); ?>/img/product-5.png" alt="product-1">
									</a>
								</div>

								<div class="shop-product-title">
									<a href="#" class="shop-link">
										Message cotton t-shirt
									</a>
								</div>

								<div class="shop-product-price">
									<span class="shop-product-price-actual">$100.00 <span class="uppercase">USD</span></span>
								</div>
						</article>
					</div>


					<div class="col-lg-3  col-md-5">
						<article class="shop-product sale">
								<div class="shop-product-img">
									<a href="#" class="shop-link">
										<img src="<?php  echo get_stylesheet_directory_uri(); ?>/img/product-6.png" alt="product-1">
									</a>
								</div>

								<div class="shop-product-title">
									<a href="#" class="shop-link">
										Sequin star t-shirt
									</a>
								</div>

								<div class="shop-product-price">
									<span class="shop-product-price-old"><del>$200.00 <span class="uppercase">USD</span></del></span>
									<span class="shop-product-price-actual">$100.00 <span class="uppercase">USD</span></span>
								</div>
						</article>
					</div>

					<div class="col-lg-3  col-md-5">
						<article class="shop-product">
								<div class="shop-product-img">
									<a href="#" class="shop-link">
										<img src="<?php  echo get_stylesheet_directory_uri(); ?>/img/product-7.png" alt="product-2">
									</a>
								</div>

								<div class="shop-product-title">
									<a href="#" class="shop-link">
										Ruffle sleeve t-shirt
									</a>
								</div>

								<div class="shop-product-price">
									<span class="shop-product-price-actual">$175.00 <span class="uppercase">USD</span></span>
								</div>
						</article>
					</div>

					<div class="col-lg-3  col-md-5">
						<article class="shop-product">
								<div class="shop-product-img">
									<a href="#" class="shop-link">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product-8.png" alt="product-3">
									</a>
								</div>

								<div class="shop-product-title">
									<a href="#" class="shop-link">
										Double layer top
									</a>
								</div>

								<div class="shop-product-price">
									<span class="shop-product-price-actual">$200.00 <span class="uppercase">USD</span></span>
								</div>
						</article>
					</div>
				</div>

				<div class="row  justify-content-md-center">
					<div class="col">
						<div class="shop-load-more">
							<button class="load-more-btn btn"><i class="fas fa-sync"></i>Load more</button>
						</div>
					</div>
				</div>
			</section>



			<section class="content-slider container-fluid">
				<div class="content-slider-inner">
					<div class="content-slider-slide">
						<div class="slide-message">
							<div class="slide-message-top">Event</div>
							<h2 class="slide-title">London Fashion Week September 2017</h2>
							<div class="message-date">15th - 19th September 2017</div>
						</div>
					</div>

					<div class="content-slider-slide">
						<div class="slide-message">
							<div class="slide-message-top">Event</div>
							<h2 class="slide-title">London Fashion Week September 2017</h2>
							<div class="message-date">15th - 19th September 2017</div>
						</div>
					</div>

					<div class="content-slider-slide">
						<div class="slide-message">
							<div class="slide-message-top">Event</div>
							<h2 class="slide-title">London Fashion Week September 2017</h2>
							<div class="message-date">15th - 19th September 2017</div>
						</div>
					</div>
				</div>
			</section>

			<section class="offer container">
				<div class="row justify-content-md-center">
					<div class="col-md-6   offer-item">
						<div class="row no-gutters">
							<div class="col-lg-6 offer-message">
								<div class="offer-cat">suits</div>

								<h3 class="offer-title">
									Slim Fit Prince of Wales Check Wool
								</h3>

								<div class="offer-price">2,295.00 USD</div>
							</div>

							<div class="col-lg-6 offer-img">
								<img src="<?php echo get_stylesheet_directory_uri()?>/img/promo-1.png" alt="promo-1">
							</div>
						</div>
					</div>

					<div class="col-md-6  offer-item">
						<div class="row no-gutters">
							<div class="col-lg-6 offer-message">
								<div class="offer-cat">Scarves</div>

								<h3 class="offer-title">
									Ligheweight check wool cashmere scarf
								</h3>

								<div class="offer-price">425.00 USD</div>
							</div>

							<div class="col-lg-6 offer-img">
								<img src="<?php echo get_stylesheet_directory_uri()?>/img/promo-2.png" alt="promo-2">
							</div>
						</div>
					</div>
				</div>
			</section>
		</main><!-- #main -->
<?php
//do_action( 'storefront_sidebar' );
get_footer();
